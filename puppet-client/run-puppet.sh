# Author: Andrew Hitchcock -- athitchc@mtu.edu -- 11/1/19
#
# This script will tell the puppet agent to pull from the puppet master
#
# -----------------------------------------------------------------------------------

#! /bin/bash

MASTER=master.local # <-- use the url of the master server here

/opt/puppetlabs/puppet/bin/puppet agent -t --server=$MASTER --debug
